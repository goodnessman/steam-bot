
var forever = require('forever-monitor');
var request = require('request');
var config            = require('./config.js');
var Winston           = require('winston');

// Setup logging to file and console
var logger = new (Winston.Logger)({
        transports: [
            new (Winston.transports.Console)({
                colorize: true, 
                level: 'debug'
            }),
            new (Winston.transports.File)({
                level: 'info', 
                timestamp: true, 
                filename: 'cratedump.log', 
                json: false
            })
        ]
});

var child = new (forever.Monitor)('app.js', {
    silent: false,
    args: []
});

child.on('restart', function() {

	var report = {
	    bot: config.admin,
	    status: 'restart',
	    mes: 'forever restarting bot'
	}

	sendJsonLog(config.url, report, function (err, response, body) {
	    if (err) {
	        logger.error('sendJsonLog: ' + err);
	    }

	    logger.error('Auto-restart bot for ' + child.times + ' time');
	});
});

child.on('exit:code', function(code) {

    var report = {
        bot: config.admin,
        status: 'stop',
        mes: 'forever stopped bot'
    }

    sendJsonLog(config.url, report, function (err, response, body) {
        if (err) {
            logger.error('sendJsonLog: ' + err);
        }

        logger.error('Forever detected script exited with code ' + code);
    });

    if (143 === code) child.stop(); // don't restart the script on SIGTERM
});

child.start();

logger.info('Forever started bot');


function sendJsonLog (url, object, callback) {
	
    var date = new Date();
    var time = date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();
    object.time = time;
    var jsonObj = JSON.stringify(object);

    jsonObj.time = time;

    request({
        url: url, //URL to hit
        qs: {from: config.admin, time: +new Date()}, //Query string data
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: jsonObj //Set the body as a string
    }, function (error, response, body) {
        callback(error, response, body)
    });
}