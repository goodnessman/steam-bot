
'use strict';

// app
var express           = require('express');
var http              = require('http');
var path              = require('path');
var bodyParser        = require('body-parser');
var Winston           = require('winston');
var async             = require('async'); // asynchron functions
var request           = require('request');

// steam-bot
var SteamUser         = require('steam-user');
var TradeOfferManager = require('steam-tradeoffer-manager');
var SteamCommunity    = require('steamcommunity');
var fs                = require('fs');
var SteamTotp         = require('steam-totp');

// moduls
var config            = require('./config.js');

// подключаем отдельный config если бот в разработке
if (config.type === 'test') {
    console.log('use config-local');

    config = require('./config-local.js');
}

// генерируем код для входа в стим акаунт
var guardCode = SteamTotp.generateAuthCode(config.identitySecretEnter);

var app = express();

// выводим текущий код в консоли
setInterval(function() {
    guardCode = SteamTotp.generateAuthCode(config.identitySecretEnter);
    // logger.info('guard: "'+guardCode+'"');
},30000) 

//socket
var http = require('http').Server(app);
var socket = require('socket.io-client')(config.socketNote); 

socket.on('connection', function(socket){
    console.log('Socket: user connection');
});

function notificationMessage(message) {
    socket.emit('noteFromBot', message);
}

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

var community = new SteamCommunity();

// Setup logging to file and console
var logger = new (Winston.Logger)({
        transports: [
            new (Winston.transports.Console)({
                colorize: true, 
                level: 'debug'
            }),
            new (Winston.transports.File)({
                level: 'info', 
                timestamp: true, 
                filename: 'cratedump.log', 
                json: false
            })
        ]
});

// Initialize the Steam client and our trading library
var client = new SteamUser();
var offers = new TradeOfferManager({
    steam:        client,
    domain:       config.domain, 
    language:     "en", // English item descriptions
    pollInterval: 10000, // (Poll every 10 seconds (10,000 ms)
    cancelTime:   300000 // Expire any outgoing trade offers that have been up for 5+ minutes (300,000 ms)
});

// We have to use application IDs in our requests--this is just a helper
var appid = {
    TF2:   440,
    DOTA2: 570,
    CSGO:  730,
    Steam: 753
};
// We also have to know context IDs which are a bit tricker since they're undocumented.
// For Steam, ID 1 is gifts and 6 is trading cards/emoticons/backgrounds
// For all current Valve games the context ID is 2.
var contextid = {
    TF2:   2,
    DOTA2: 2,
    CSGO:  2,
    Steam: 6
}

// If we've run this before, we should have a saved copy of our poll data.
// We can load this up to gracefully resume polling as if we never crashed/quit
fs.readFile('polldata.json', function(err, data) {
    var logMessage = {};

    if (err) {
        logMessage.error = 'Error reading polldata.json. If this is the first run, this is expected behavior: '+err;
        
        sendJsonLog(config.url, logMessage, function() {
            logger.warn(logMessage.error);
        });
    } else {
        if (data.length === 0) {

            fs.unlink('polldata.json', function(err) {
              if (err) return logger.error('unlink file: '+err);

                logMessage.error = 'Error: not valid polldata';

                sendJsonLog(config.url, logMessage, function() {
                    logger.error(logMessage.error);
                });
                process.kill(process.pid, 'SIGKILL'); // forever to restart the script
            });
        }
        logMessage.info = 'Found previous trade offer poll data.  Importing it to keep things running smoothly.';
        logMessage.error = '';

        sendJsonLog(config.url, logMessage, function() {
            logger.debug(logMessage.info);
        });
        offers.pollData = JSON.parse(data);
    }
});
    
// Sign into Steam
loginUserInSteam(config.username, config.password, guardCode);

client.on('loggedOn', function (details) {
    var logMessage = {};
    
    logMessage.info = "Logged into Steam as " + client.steamID.getSteam3RenderedID();

    sendJsonLog(config.url, logMessage, function() {
        logger.info(logMessage.info);
    });
    // If you wanted to go in-game after logging in (for crafting or whatever), you can do the following
    // client.gamesPlayed(appid.TF2);
    var myId = client.steamID;
    logger.debug('steamID ' + myId);
    

    // logger.debug('getSteam3RenderedID ' + id64.id.getSteam3RenderedID());
});

client.on('error', function(e) {
    var logMessage = {};
    
    // Some error occurred during logon.  ENums found here: 
    // https://github.com/SteamRE/SteamKit/blob/SteamKit_1.6.3/Resources/SteamLanguage/eresult.steamd
    logMessage.error = e;

    sendJsonLog(config.url, logMessage, function() {
        logger.error(logMessage.error);
    });
    process.exit(1);
});

client.on('webSession', function (sessionID, cookies) {
    var logMessage = {};

    logger.debug("Got web session");
    // Set our status to "Online" (otherwise we always appear offline)
    client.friends.setPersonaState(SteamUser.Steam.EPersonaState.Online);
    offers.setCookies(cookies, function (err){
        if (err) {
            logMessage.error = 'Unable to set trade offer cookies: '+err;

            sendJsonLog(config.url, logMessage, function() {
                logger.error(logMessage.error);
            });
            process.exit(1); // No point in staying up if we can't use trade offers
        }else {
            var report = {bot_status: 'ON'}

            logger.debug("Trade offer cookies set.  Got API Key: "+offers.apiKey);

            setTimeout(function() {
                sendJsonLog(config.url, report, function () {
                    logger.info("bot send status ON to server");
                });
            }, 10000)

            consolStatusBot();

            app.get('/', function (req, res) {
               res.end('Bot is running!');
            });

            app.post('/restartBot', function (req, res) {
               process.kill(process.pid, 'SIGKILL');
               res.end();
            });

            app.post('/login', function (req, res) {
               loginUserInSteam(client, config.username, config.password, guardCode);
               res.end();
            });

            app.post('/logoff', function (req, res) {
               client.logOff();
               res.end();
            });

            app.post('/getBotStatus', function (req, res) {
                var report = {bot_status: 'ON'};
                var note = {mess: 'bot status - ON',type: 'success'};

                sendJsonLog(config.url, report, function () {
                    logger.info('bot status - ON (server log)');
                });

                responseJson(report, res);

                notificationMessage(note);
            });

            app.post('/checkSendRequest', function (req, res) {
                
                checkSendRequest(function (status) {
                    var report = {};
                    var log = '';

                    if (status === 'suckess') {
                        report.bot_status = 'BOT_CAN_SEND_ITEM';
                        logger.info('checkSendRequest: bot can send item (server log);');

                    }else {

                        if (status.message === 'Invalid protocol: steammobile:') {
                            report.bot_status = 'ERROR_BOT_CAN_NOT_SEND_ITEM';

                            sendJsonLog(config.url, report, function () {

                                logger.error('checkSendRequest: bot can not send item (server log)');
                                responseJson(report, res);

                                process.kill(process.pid, 'SIGKILL');
                            });
                            
                        }else {
                            report.bot_status = 'ERROR_BOT_CAN_NOT_SEND_ITEM';
                            logger.error('checkSendRequest: bot can not send item '+status+' (server log);');
                        } 
                    }

                    sendJsonLog(config.url, report, function() {});

                    report.bot_status = 'BOT_CAN_SEND_ITEM';

                    responseJson(report, res); 
                });
            });
                
            
            app.post('/sendToUser', function (req, res) {

                if (checkGetData(req.body, res)) {
                    var data = req.body;

                    responseJson({ offer_status: 'process'}, res); //end request

                    sendItem(data.userId, data.userToken, data.itemIds, data.gameId, data.systemTradeId);
                }
            });


            app.post('/getFromUser', function (req, res) {

                if (checkGetData(req.body, res)) {
                    var data = req.body;

                    responseJson({ offer_status: 'process'}, res); //end request

                    getItem(data.userId, data.userToken, data.itemIds, data.gameId, data.systemTradeId);
                }
            });
        }
    });
    

    community.setCookies(cookies, function (err){
        var logMessage = {};

        if (err) {
            logMessage.error = 'Unable to set community cookies: '+err;

            sendJsonLog(config.url, logMessage, function() {
                logger.error(logMessage.error);
            });
            process.exit(1); // No point in staying up if we can't use trade offers
        }
    });

    community.loggedIn(function (err, loggedIn, familyView){
        var logMessage = {};

        if (err){
            logMessage.error = 'Error logining in community '+err;

            sendJsonLog(config.url, logMessage, function() {
                logger.error(logMessage.error);
            });
        }
        if (loggedIn) {

            logger.info('User is login in community');
        }else {
            logMessage.error = 'User not login in community';

            sendJsonLog(config.url, logMessage, function() {
                logger.error(logMessage.error);

                process.kill(process.pid, 'SIGKILL');
            });
        }
    });

    
});

// Emitted on login and when email info changes
// Not important in our case, but kind of neat.
client.on('emailInfo', function (address, validated) {
    
    logger.info("Our email address is " + address + " and it's " + (validated ? "validated" : "not validated"));
});

// Looking at your account limitations can be very useful depending on what you're doing
client.on('accountLimitations', function (limited, communityBanned, locked, canInviteFriends) {
    var logMessage = {};

    if (limited) {
        // More info: https://support.steampowered.com/kb_article.php?ref=3330-IAGK-7663
        logMessage.error = "Our account is limited. We cannot send friend invites, use the market, open group chat, or access the web API.";

        sendJsonLog(config.url, logMessage, function() {
            logger.error(logMessage.error);
        });
    }
    if (communityBanned){
        // More info: https://support.steampowered.com/kb_article.php?ref=4312-UOJL-0835
        // http://forums.steampowered.com/forums/showpost.php?p=17054612&postcount=3
        logMessage.error = "Our account is banned from Steam Community";

        sendJsonLog(config.url, logMessage, function() {
            logger.error(logMessage.error);
        });
        // I don't know if this alone means you can't trade or not.
    }
    if (locked){
        // Either self-locked or locked by a Valve employee: http://forums.steampowered.com/forums/showpost.php?p=17054612&postcount=3
        logMessage.error = "Our account is locked. We cannot trade/gift/purchase items, play on VAC servers, or access Steam Community.  Shutting down.";

        sendJsonLog(config.url, logMessage, function() {
            logger.error(logMessage.error);
        });
        process.exit(1);
    }
    if (!canInviteFriends){
        // This could be important if you need to add users.  In our case, they add us or just use a direct tradeoffer link.
        logMessage.error = "Our account is unable to send friend requests.";

        sendJsonLog(config.url, logMessage, function() {
            logger.error(logMessage.error);
        });
    }
});



// When an offer sent by someone else changes states
offers.on('receivedOfferChanged', function (offer, oldState) {
    logger.info(offer.partner.getSteam3RenderedID() +" Offer #" + offer.id + " changed: " + TradeOfferManager.getStateName(oldState) + " -> " + TradeOfferManager.getStateName(offer.state));

    // Alert us when we accept an offer
    if (offer.state == TradeOfferManager.ETradeOfferState.Accepted) {
        offer.getReceivedItems(function (err, items) {
            if (err) {
                logger.error("Couldn't get received items: " + err);

                report.offer_status = 'ERROR_RECEIVED_OFFER';

                // отсылает ответ на сервер
                sendStatusTradeToServer(report);
            } else {
                var names = items.map(function(item) {
                    return item.name;
                });
                // Log a comma-separated list of items received
                logger.info("Received: " + names.join(', '));

                report.offer_status = 'RECEIVED_OFFER';

                // отсылает ответ на сервер
                sendStatusTradeToServer(report);
            }
        });
    }
});

// When one of our offers changes states
offers.on('sentOfferChanged', function (offer, oldState) {
    // Alert us when one of our offers is accepted

    var report = {
        offer_status: 'OFFER_CHENGE_STATUS',
        offer_status_id: offer.state,
        partner_id: offer.partner.getSteamID64(),
        offer_id: offer.id
        // itemsToReceive: parseInventoriItem(offer.itemsToReceive)
    };

    logger.info('Offer.stete', offer.state);
    logger.info('oldState', oldState);

    if (offer.state == TradeOfferManager.ETradeOfferState.Accepted) {
        logger.info("Our sent offer #"+ offer.id + " has been accepted.");

        report.offer_status = 'OFFER_ACCEPTED';

        // отсылает ответ на сервер
        sendStatusTradeToServer(report);
    }else {

        // отсылает ответ на сервер
        sendStatusTradeToServer(report);
    }

});

// Steam is down or the API is having issues
offers.on('pollFailure', function (err) {
    var logMessage = {};

    logMessage.error = "Error polling for trade offers: "+err;

    sendJsonLog(config.url, logMessage, function() {
        logger.error(logMessage.error);
        process.kill(process.pid, 'SIGKILL');
    });
});

// When we receive new trade offer data, save it so we can use it after a crash/quit
offers.on('pollData', function (pollData) {
    fs.writeFile('polldata.json', JSON.stringify(pollData));
});

// -----------------------

function getItem(userId, userToken, itemIds, gameNumber, systemTradeId) {
    var SteamID = TradeOfferManager.SteamID;
    var sid = new SteamID(userId);
    var trade = offers.createOffer(sid);
    var note;
    var report = {trade_id: systemTradeId};

    loadPartnerInventory(trade, gameNumber, report, function (inventory) {
        findItems(inventory, itemIds, function (item) {
            
            if (item.length === itemIds.length) {
                logger.info('Item in partner inventory: '+inventory.length);

                trade.addTheirItems(item);

                trade.send('New offer', userToken, function (err, status) {
                    report.offer_id = trade.id;
                    report.partner_id = trade.partner.getSteamID64();
                    report.itemsToReceive = itemIds;
                    report.itemsToReceive[0].appid = gameNumber;

                    if (err) {


                        report.offer_status = 'ERROR_OFFER_SENT';

                        note = {mess: 'offer #'+trade.id+' error sent',type: 'error'};

                        sendJsonLog(config.url, report, function () {
                            logger.error('Trade offer #'+trade.id+' error sent (serrver log);');
                        });

                        // отсылает ответ на сервер
                        sendStatusTradeToServer(report);

                        notificationMessage(note);

                        if (err.eresult == 15) {
                            logger.err('vrong offer data');
                        }else {
                            process.kill(process.pid, 'SIGKILL');
                        }

                        
                    }else {
                        report.offer_status = 'OFFER_SENT';

                        note = {mess: 'offer #'+trade.id+' sent success', type: 'success'};

                        sendJsonLog(config.url, report, function () {
                            logger.info('Trade offer #'+trade.id+' sent success (serrver log);');
                        });

                        // отсылает ответ на сервер
                        sendStatusTradeToServer(report);

                        notificationMessage(note);
                    };
                });

            }else {
                report.offer_status = 'ERROR_NOT_FOUND_ITEM_IN_INVERTORY';

                note = {mess: 'not found item in partner inventory',type: 'error'};

                sendJsonLog(config.url, report, function () {
                    logger.error('Not found item in partner inventory (serrver log);');
                });

                // отсылает ответ на сервер
                sendStatusTradeToServer(report);

                notificationMessage(note);

                return true;
            }   
        });
    });
};

function sendItem(userId, userToken, itemIds, gameNumber, systemTradeId) {
    var SteamID = TradeOfferManager.SteamID;
    var sid = new SteamID(userId);
    var trade = offers.createOffer(sid);
    var note = {};
    var report = {trade_id: systemTradeId};

    loadMyInventory(gameNumber, report, function (inventory) {
        findItems(inventory, itemIds, function (item) {
            
            if (item.length === itemIds.length) {
                logger.info('Item in your inventory: '+inventory.length);

                trade.addMyItems(item);

                trade.send('New offer', userToken, function (err, status) {
                    report.offer_id = trade.id;
                    report.partner_id = trade.partner.getSteamID64();
                    report.itemsToReceive = itemIds;
                    report.itemsToReceive[0].appid = gameNumber;

                    if (err) {
                        
                        report.offer_status = 'ERROR_OFFER_SENT';

                        note = {mess: 'offer #'+trade.id+' error sent',type: 'error'};

                        sendJsonLog(config.url, report, function () {
                            logger.error('Trade offer #'+trade.id+' error sent (serrver log);');
                        });

                        sendStatusTradeToServer(report);

                        if (err.eresult == 15) {
                            logger.err('vrong offer data');
                        }else {
                            process.kill(process.pid, 'SIGKILL');
                        }
                    }else {
                        report.offer_status = 'OFFER_CREATE';

                        note = {mess: 'offer #'+trade.id+' sent but awaiting confirmation',type: 'warning'};

                        sendJsonLog(config.url, report, function () {
                            logger.warn('Trade offer #'+trade.id+' sent but awaiting confirmation (serrver log);');
                        });

                        notificationMessage(note);

                        managerConfirmation(trade.id, report);
                    }
                });

            }else {
                report.offer_status = 'ERROR_NOT_FOUND_ITEM_IN_INVERTORY';
                
                note = {mess: 'not item for trade',type: 'error'};

                sendJsonLog(config.url, report, function () {
                    logger.error('Not item for trade (serrver log);');
                });

                sendStatusTradeToServer(report);

                notificationMessage(note);
            }   
        });
    });
};

// Управление подтверждением предложения
function managerConfirmation(tradeId, report) {
    var timer = 0;
    var intervalID;
    var note;
    var errConfirm;

    intervalID = setInterval(function() {
        timer++;

        if (timer === 5) {
            report.offer_status = 'ERROR_CONFIRM_OFFER';

            note = {mess: 'offer #'+tradeId +' error confirm',type: 'error'};

            sendJsonLog(config.url, report, function () {
                logger.error('Confirmations ' + errConfirm+ ' (serrver log);');
            });

            notificationMessage(note);

            sendStatusTradeToServer(report);

            clearInterval(intervalID);
        }else {

            logger.info('Trying to confirm offer #' + tradeId+ ' '+timer+' time');

            getConfirm(tradeId, function(err) {
                if (err) {
                    logger.error('Error confirm offer #'+tradeId+': ' + err.message);

                    errConfirm = err.message;

                    // if (err.message === 'Invalid protocol: steammobile:') {
                    //     // report = {offer_status: 'ERROR_CONFIRM_OFFER'}

                    //     // sendJsonLog(config.url, report, function () {
                    //     //     logger.error('Error confirm offer #'+tradeId+': ' + err.message +' (serrver log);');
                    //     // });

                    //     // responseJson(report, res);

                    //     // process.kill(process.pid, 'SIGKILL');

                    //     logger.error('Error confirm offer #'+tradeId+': ' + err.message +' (serrver log);');
                    // }
                }else {
                    report.offer_status = 'OFFER_CONFIRM_SUCCESS';

                    note = {mess: 'offer #'+tradeId +' confirm success',type: 'success'};

                    sendJsonLog(config.url, report, function () {
                        logger.info('Success confirm offer #'+tradeId +' (serrver log);');
                    });

                    sendStatusTradeToServer(report);

                    notificationMessage(note);

                    clearInterval(intervalID);
                }
            });
        }
        
    }, 10000);
};

function getConfirm(tradeId, callback) {
    var timekey=Math.round(Date.now() / 1000);
    var confirmationkey = SteamTotp.getConfirmationKey(config.identitySecret, timekey, "conf");

    community.getConfirmations(timekey, confirmationkey, function (err, confirmations) {
        if (err) {
            
            callback(err);
        }else {

            if (confirmations.length > 0) {

                    confirmations.forEach(function (conf, index, array) {
                    setTimeout(function (conf) { 

                        var time = Math.floor(Date.now() / 1000);
                        var key = SteamTotp.getConfirmationKey(config.identitySecret, time, 'details');

                        conf.getOfferID(time, key, function(err, offerID) {
                            logger.info('confirmation in stack: #'+offerID);

                            if (tradeId === offerID) {
                                var timekey2=Math.round(Date.now() / 1000);
                                var confirmationkey2 = SteamTotp.getConfirmationKey(config.identitySecret, timekey2, "allow");

                                community.respondToConfirmation(conf.id, conf.key, timekey2, confirmationkey2, true, function (err) {

                                    callback();
                                });
                            };
                        });
                    },index*1500, conf);
                });
            };
        };
    });
};

function loadPartnerInventory (trade, gameNumber, report, callback) {
    var contextNumber = 2;

    if (gameNumber === 753) {
        contextNumber = 1;
    };

    trade.loadPartnerInventory(gameNumber, contextNumber, function (err, inventory) {
        report.offer_status = 'ERROR_LOAD_INVENTORY';

        if (err) {
            logger.error('loadPartnerInventory '+err);

            sendStatusTradeToServer(report);

            return true;
        }

        callback(inventory);
    });
};

function loadMyInventory (gameNumber, report, callback) {
    var contextNumber = 2;

    if (gameNumber === 753) {
        contextNumber = 1;
    };

    offers.loadInventory(gameNumber, contextNumber, true, function (err, inventory) {

        if (err) {
            logger.error('loadMyInventory '+err);

            sendStatusTradeToServer(report);

            return true;
        }

        callback(inventory);
    });
};

function findItems (inventory, itemId, callback) {
    var items = [];
    var arr = inventory;

    for(var i = 0; i < itemId.length; i++) {

        if (i === 0) {
          arr.forEach(function (item, index) {
              console.log('item-'+(index+1)+': '+item.classid+'  inst: '+item.instanceid);
          });
        }
        
        for (var u = 0; u < arr.length; u++) {
          if (arr[u].classid === itemId[i].classid && arr[u].instanceid === itemId[i].instanceid) {
                logger.debug('classid: '+arr[u].classid+' instanceid: '+arr[u].instanceid);
                items.push(arr[u]);
                arr.splice(u, 1);
                break;
          }
        }
    }
    callback(items);
};

// get classId from array item
function getClassId (items) {
    var itemsResult = [];
    // Object.keys(itemsRusult).length;

    for(var i = 0; i < items.length; i++) {
        itemsResult.push(items[i].classid);
    }
    return itemsResult;
};

// heck data get from rost request
function checkGetData(data, res) {
    console.log(data);
    var report = {offer_status: 'ERROR_VALIDATION_INPUT_DATA'}; 

    if (!data.userId || !(data.userId.length > 0)) {
        
        sendErrorValidationLog('userId', res);
        responseJson(report, res);
        return false;
    }

    if (!data.systemTradeId || !(data.systemTradeId.length > 0)) {
        
        sendErrorValidationLog('systemTradeId', res);
        responseJson(report, res);
        return false;
    }

    if (!data.userToken || !(data.userToken.length > 0)) {
        
        sendErrorValidationLog('userToken', res);
        responseJson(report, res);
        return false;
    }

    if (!data.itemIds || !(data.itemIds.length > 0)) {
        sendErrorValidationLog('itemIds', res);
        responseJson(report, res);
        return false;
    }

    console.log(data.gameId);
    console.log(typeof data.gameId);

    // if (!data.gameId || !(data.gameId.length > 0)) {
    //     sendErrorValidationLog('gameId', res);
    //     responseJson(report, res);
    //     return false;
    // }

   console.log('gameId type: ' + typeof data.gameId);
    
    if (data.gameId === 440 || data.gameId === 570 || data.gameId === 730 || data.gameId === 753) {
        return true;
    }else {
        sendErrorValidationLog('gameId', res);
        responseJson(report, res);
        
        return false;
    }
};

function sendErrorValidationLog(field, res) {
    var report = {offer_status: 'ERROR_VALIDATION_INPUT_DATA'}

    sendJsonLog(config.url, report, function () {
       
    logger.error('Get not valid data "'+field+'"');
        
    });
};

// формирует JSON для ответа серверу
function responseJson(object, res) {
    var date = new Date();
    var jsonObj = {};

    object.time = date.toString();
    object.bot_id = config.admin;
    object.bot_type = config.type;

    jsonObj = JSON.stringify(object);

    res.end(jsonObj);
};

// посылает логи на сервер
function sendJsonLog (url, object, callback) {
    var date = new Date();
    object.time = date.toString();
    object.bot_id = config.admin;
    object.bot_type = config.type;

    var jsonObj = JSON.stringify(object);

    request({
        url: url, //URL to hit
        qs: {from: config.admin, time: +new Date()}, //Query string data
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: jsonObj //Set the body as a string
    }, function (err) {
        if (err) {
            logger.error('sendJsonLog: ' + err.message);
        }else {

            callback();
        }
    });
};

// посылает ответ по статусу трейда 
function sendStatusTradeToServer(object) {
    var date = new Date();
    var jsonObj;
    var url;

    object.time     = date.toString();
    object.bot_id   = config.admin;
    object.bot_type = config.type;

    jsonObj = JSON.stringify(object);
    
    if (config.type === 'test') {
        url = config.ansverServerTest;
    }else {
        url = config.ansverServer;
    }

    console.log('request: ', jsonObj);

    request({
      uri: url,
      method: "POST",
      form: {
        request: jsonObj
      }
    }, function(error, response, body) {
        if (error) {
            logger.error('sendJsonLog: ' + err.message);
        }
        console.log(body);
    });


    // request.post(url, object, function(err, res, body) {

    //     if (err) {
    //         logger.error('sendJsonLog: ' + err.message);
    //     }else {
    //         console.log(jsonObj);
    //     }
        
    //     console.log(res.statusCode);
    //     console.log(body);
    // });

    // request({
    //     url: url, // URL to hit
    //     // qs: {from: config.admin, time: +date}, //Query string data
    //     method: 'POST',
    //     // headers: {
    //     //     'Content-Type': 'application/json'
    //     // },
    //     body: {body : jsonObj } // Set the body as a string
    // }, function (err) {
    //     if (err) {
    //         logger.error('sendJsonLog: ' + err.message);
    //     }else {
    //         console.log(jsonObj);
    //     }
    // });
};

// Парсит обект елемента для передачи в ответ серверу
function parseInventoriItem(arr) {
    var i;
    var resData = [];

    // console.log(arr);

    if (arr.length > 0) {
        for(i = 0; i < arr.length; i++) {
            resData.push({});

            resData[0].appid = arr[i].appid;
            resData[0].classid = arr[i].classid;
            resData[0].instanceid = arr[i].instanceid;
            // resData[0].amount = arr[i].amount;
        }
    }

    return resData;
};

// Пишет статус бота в консоль
function consolStatusBot() {
    setInterval(function() {
        var date = new Date();

        // logger.info('bot running'+' ('+date.toString()+')');
    }, 60000);
};

// Проверяет может ли бот отправить вещь
function checkSendRequest(callback) {
    var timekey=Math.round(Date.now() / 1000);
    var confirmationkey = SteamTotp.getConfirmationKey(config.identitySecret, timekey, "conf");

    community.getConfirmations(timekey, confirmationkey, function (err, confirmations) {
        if (err) {
            callback(err);
        }else {
            callback('suckess');
        }
    });
};

// Sign into Steam
function loginUserInSteam(username, password, guardCode) {
    client.logOn({
        accountName: config.username,
        password: config.password,
        authCode: guardCode,
        twoFactorCode: guardCode
    });
};

var port = process.env.PORT || config.port;

http.listen(port, function () {
  console.log('App listening on port ' + port + '!');
});